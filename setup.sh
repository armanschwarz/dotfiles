#!/bin/bash
usr=$1
set -e

dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# create simlinks
ln -fs $dir/bashrc $HOME/.bashrc
ln -fs $dir/gitconfig $HOME/.gitconfig
ln -fs $dir/inputrc $HOME/.inputrc
ln -fs $dir/tmux.conf $HOME/.tmux.conf
ln -fs $dir/tmux_battery.sh $HOME/.tmux_battery.sh
ln -fs $dir/vimrc $HOME/.vimrc

# rebuild vim directory
rm -rf $HOME/.vim 2> /dev/null
curl -fLo $HOME/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

# install vim plugins
vim +PlugInstall +qall

# get pip
wget https://bootstrap.pypa.io/pip/3.6/get-pip.py --no-check-certificate -P /tmp/ && python3 /tmp/get-pip.py --user
pip3 install black flake8 mypy yamllint --user

# assume npm is installed
npm install --prefix ~/.local/ eslint@7 eslint-plugin-vue

ln -fs $dir/filetype.vim $HOME/.vim/filetype.vim

chown -R $usr:$usr $HOME/.vim
