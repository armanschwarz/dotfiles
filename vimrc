set nocompatible
filetype off
filetype indent plugin on

set matchpairs+=<:>
syntax on
syntax sync minlines=20
autocmd BufEnter * syntax sync fromstart

" set textwidth=79
set autoindent
set expandtab
set smarttab
set number
set hlsearch
set incsearch
set tags=./tags;

set backspace=indent,eol,start

set path+=/usr/local/include
set path+=/usr/include

call plug#begin('~/.vim/plugged')
Plug 'danro/rename.vim'
Plug 'tomtom/tcomment_vim'
Plug 'tmhedberg/matchit'
Plug 'tpope/vim-fugitive'
Plug 'dense-analysis/ale'
Plug 'itchyny/lightline.vim'
Plug 'flazz/vim-colorschemes'
Plug 'xolox/vim-colorscheme-switcher'
Plug 'xolox/vim-misc'
Plug 'posva/vim-vue'
Plug 'preservim/nerdtree'
call plug#end()

filetype plugin indent on    " required

" nerdtree
nnoremap <leader>n :NERDTreeFocus<CR>
nnoremap <C-n> :NERDTree<CR>
nnoremap <C-t> :NERDTreeToggle<CR>
nnoremap <C-f> :NERDTreeFind<CR>

" multiple pastes
xnoremap p pgvy

let &colorcolumn=88
cabbr <expr> %% expand('%:p:h')

set visualbell

" silent so that it doesn't fail dring first load before themes are installed
silent! colorscheme desertEx

let g:ale_linters = {'python' : ['flake8', 'mypy'], 'javascript' : ['eslint'], 'vue' : ['prettier', 'eslint'], 'yaml' : ['yamllint']}
let g:ale_fixers = {'python' : ['black'], 'javascript' : ['prettier', 'eslint'], 'vue' : ['prettier', 'eslint']}
let g:ale_python_flake8_executable = 'python3 -B -m flake8'
let g:ale_python_flake8_options = '--max-line-length 88 --ignore E203 --ignore E266 --ignore E501 --ignore W503 --select B,C,E,F,W,T4,B9 --max-complexity 18'
let g:ale_javascript_eslint_options = '--rule "camelcase:1"'

let g:ale_fix_on_save = 1
let g:ale_sign_error = 'E'
let g:ale_sign_warning = 'W'

nmap <silent> <leader>aj :ALENext<cr>
nmap <silent> <leader>ak :ALEPrevious<cr>

set laststatus=2

set noswapfile " more trouble than it's worth

set sw=4 ts=4 sts=4

autocmd BufRead,BufNewFile *.ts.vue setfiletype typescript
autocmd FileType html set sw=2 ts=2 sts=2
autocmd FileType vue set sw=2 ts=2 sts=2
autocmd FileType python set sw=4 ts=4 sts=4
autocmd FileType javascript set sw=2 ts=2 sts=2

if &term =~ '256color'
    " Disable Background Color Erase (BCE) so that color schemes
    " work properly when Vim is used inside tmux and GNU screen.
    set t_ut=
endif
