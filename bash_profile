if [ -f ~/.git-completion.bash ]; then
  . ~/.git-completion.bash
fi

export PATH=$HOME/.local/bin:$PATH

# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
    . "$HOME/.bashrc"
    fi
fi
