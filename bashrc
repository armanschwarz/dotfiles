export EDITOR=vim
alias vi=vim
alias ls='ls --color=auto'

set editing-mode vi

[ -f ~/.fzf.bash ] && source ~/.fzf.bash
export TERM='xterm-256color'

COLOUR1="\[$(tput setaf 244)\]"
_RESET="\[$(tput sgr0)\]"
_COLOUR2="\[$(tput setaf 017)\]"
_EMPHASISE="\[$(tput bold)\]"

export PS1="${COLOUR1}\h${_COLOUR2}@${COLOUR1}\u${_RESET}${_COLOUR2}: ${_RESET}${_EMPHASISE}\w${_RESET} \$ "
