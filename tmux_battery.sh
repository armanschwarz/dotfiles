#!/bin/bash

power=$(cat /sys/class/power_supply/BAT0/capacity)
is_charging=$(cat /sys/class/power_supply/AC/online)

symbol=""
if [ "$is_charging" == "1" ] ; then
    symbol="+%"
elif [ "$is_charging" != "0" ] ; then
    symbol="" # we don't recognise this symbol
else
    symbol="%"
fi

echo $power$symbol
