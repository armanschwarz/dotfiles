" my filetype file
if exists("did_load_filetypes")
    finish
endif
augroup filetypedetect
    " add rupal extensions
    au! BufRead,BufNewFile *.module setfiletype php
    au! BufRead,BufNewFile *.install setfiletype php
augroup END
